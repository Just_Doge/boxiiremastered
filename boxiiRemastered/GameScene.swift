//
//  GameScene.swift
//  boxiiRemastered
//
//  Created by Geddy on 26/5/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import SpriteKit
import GameplayKit

var block = SKSpriteNode()
var blockSize = CGSize(width: 90, height: 90)

var touchedNode = SKNode()

var lblMain = SKLabelNode()
var lblScore = SKLabelNode()
var lblTimer = SKLabelNode()

var offBlackColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)
var offWhiteColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
var whiteCustomColor = UIColor.white
var orangeCustomColor = UIColor.orange

var isBeingPlaced = true
var isCollecting = false
var isComplete = false

var countDownTimerPlaced = 7
var countDownTimerCollected = 5

var score = 0

var touchLocation = CGPoint()



class GameScene: SKScene {
    override func didMove(to view: SKView) {
        self.backgroundColor = offBlackColor
        resetVariables()
        spawnLblMain()
        spawnLblScore()
        spawnLblTimer()
        
        countDownTimerIsPlacing()
    }
    
    func resetVariables() {
        isBeingPlaced = true
        isCollecting = false
        isComplete = false
        
        countDownTimerPlaced = 7
        countDownTimerCollected = 5
        
        score = 0

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches {
            touchLocation = t.location(in: self)
            if isBeingPlaced && isCollecting == false {
                spawnBlock()
                score += 1
                updateScore()
            }
            
            if isCollecting && isBeingPlaced == false {
                touchedNode = atPoint(touchLocation)
                
                if touchedNode.name == "blockName" {
                    score += 1
                    updateScore()
                    touchedNode.removeFromParent()
                    
                }
                
                
            }
        }
    }
    
    func spawnBlock() {
        block = SKSpriteNode(imageNamed: "whiteCircle")
        block.size = blockSize
        
        
        block.position = touchLocation
        
        block.name = "blockName"
        
        animateBlocks()
        self.addChild(block)
    }
    
    func animateBlocks() {
        let rotate = SKAction.rotate(byAngle: 1, duration: 1.0)
        block.run(SKAction.repeatForever(rotate))
    }
    
    func spawnLblMain() {
        lblMain = SKLabelNode(fontNamed: "Futura")
        lblMain.fontSize = 90
        lblMain.fontColor = offWhiteColor
        lblMain.position = CGPoint(x: self.frame.midX, y: self.frame.midY + 250)
        lblMain.text = "Place Blocks!"
        self.addChild(lblMain)
        
    }
    
    func spawnLblScore() {
        lblScore = SKLabelNode(fontNamed: "Futura")
        lblScore.fontSize = 50
        lblScore.fontColor = offWhiteColor
        lblScore.position = CGPoint(x: self.frame.midX, y: self.frame.midY - 350)
        lblScore.text = "score: \(score)"
        self.addChild(lblScore)
        
    }
    
    func spawnLblTimer() {
        lblTimer = SKLabelNode(fontNamed: "Futura")
        lblTimer.fontSize = 100
        lblTimer.fontColor = offWhiteColor
        lblTimer.position = CGPoint(x: self.frame.midX, y: self.frame.midY - 250)
        lblTimer.text = "10"
        self.addChild(lblTimer)
        
    }
    
    func countDownTimerIsPlacing() {
        let wait = SKAction.wait(forDuration: 1.0)
        let countDown = SKAction.run {
            countDownTimerPlaced -= 1
            
            lblTimer.zPosition = 1
            
            if isBeingPlaced {
                if countDownTimerPlaced <= 5 {
                    lblTimer.text = "\(countDownTimerPlaced)"
                }
                
                if countDownTimerPlaced <= 0 {
                    self.countDownTimerIsCollecting()
                    isBeingPlaced = false
                    isCollecting = true
                                    }
            }
        }
        
        let sequence = SKAction.sequence([wait, countDown])
        self.run(SKAction.repeat(sequence, count: countDownTimerPlaced))
        
    }
    
    func countDownTimerIsCollecting() {
        let wait = SKAction.wait(forDuration: 1.0)
        let countDown = SKAction.run {
            countDownTimerCollected -= 1
            
            lblMain.fontSize = 60
            lblMain.text = "Collect Blocks!"
            lblMain.fontColor = orangeCustomColor
            lblTimer.fontColor = orangeCustomColor
            lblScore.fontColor = orangeCustomColor
            self.changeColorOfBlocks()

            if isCollecting {
                if countDownTimerCollected <= 3 {
                    lblTimer.text = "\(countDownTimerCollected)"
                }
                
                if countDownTimerCollected <= 0 {
                    isComplete = true
                    self.gameOverLogic()
                }
            }
            
        }
        
        let sequence = SKAction.sequence([wait, countDown])
        self.run(SKAction.repeat(sequence, count: countDownTimerCollected))
    }
    
    func changeColorOfBlocks() {
        if isCollecting {
            self.enumerateChildNodes(withName: "blockName", using: {node, stop in
                if let sprite = node as? SKSpriteNode {
                    sprite.texture = SKTexture(imageNamed: "orangeCircle")
                }
                
            })
        }
    }
    
    func gameOverLogic() {
        isBeingPlaced = false
        isCollecting = false
        isComplete = true
        
        
        if isComplete {
            self.enumerateChildNodes(withName: "blockName", using: {node, stop in
                if let sprite = node as? SKSpriteNode {
                    sprite.removeFromParent()
                }
                
            })
        }
        
        lblMain.fontSize = 80
        lblMain.text = "Time's Up!"
        lblTimer.text = ""
        lblMain.fontColor = offWhiteColor
        lblTimer.fontColor = offWhiteColor
        lblScore.fontColor = offWhiteColor
        resetGame()

    }
    
    func resetGame() {
        let wait = SKAction.wait(forDuration: 3.0)
        let theGameScene = GameScene(size: self.size)
        let theTransition = SKTransition.crossFade(withDuration: 0.5)
        theGameScene.scaleMode = .aspectFill
        
        let changeScene = SKAction.run {
            self.scene?.view?.presentScene(theGameScene, transition: theTransition)
        }
        
        let sequence = SKAction.sequence([wait, changeScene])
        self.run(SKAction.repeat(sequence, count: 1))
        
    }
    
    func updateScore() {
        lblScore.text = "Score: \(score)"
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
